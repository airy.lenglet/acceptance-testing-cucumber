package me.lenglet.web.controller;

import me.lenglet.dao.BookDao;
import me.lenglet.dao.BookRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {

    private final BookRepository bookRepository;

    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @GetMapping(value = "/api/books/{id}", produces = "application/json")
    public ResponseEntity<BookDao> get(@PathVariable("id") int id) {
        return ResponseEntity.of(bookRepository.findById(id));
    }

}
