package me.lenglet.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IsAliveController {

    @GetMapping(value = "/api/isAlive", produces = "application/json")
    public IsAliveResponse get() {
        return new IsAliveResponse();
    }

    public static class IsAliveResponse {

        public Boolean isAlive = true;
    }
}
