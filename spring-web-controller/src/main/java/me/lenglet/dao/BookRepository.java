package me.lenglet.dao;

import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<BookDao, Integer> {
}
