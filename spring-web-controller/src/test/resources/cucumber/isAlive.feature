Feature: the application start and is alive
  Scenario: client makes call to GET /isAlive
    When the client calls /isAlive
    Then the client receives status code of 200
    And the client receives true