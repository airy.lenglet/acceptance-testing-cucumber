Feature: the application fetch book by ID
  Scenario: client seek a known book
    When the client calls /api/books/:id with id 1
    Then the client receives status code of 200
    And the client receives the book

  Scenario: client seek an unknown book
    When the client calls /api/books/:id with id 45678
    Then the client receives status code of 404