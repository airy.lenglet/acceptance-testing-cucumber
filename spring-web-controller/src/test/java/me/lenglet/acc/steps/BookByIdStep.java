package me.lenglet.acc.steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import me.lenglet.acc.CucumberSpringApplicationRunner;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.junit.Assert.assertEquals;

public class BookByIdStep {

    private CucumberSpringApplicationRunner server;

    @Before
    public void setUp() {
        server = new CucumberSpringApplicationRunner();
        server.start();
    }

    @When("^the client calls /api/books/:id with id (\\d+)$")
    public void the_client_issues_GET_book(int id) throws Throwable {
        server.get("/api/books/" + id);
    }

    @And("^the client receives the book")
    public void the_client_receives_server_version_body() throws Throwable {
        assertEquals(200, server.response().getStatusCode());
        matchesJsonSchema(this.getClass().getResource("/json-schema/book.json"))
                .matches(server.response().body().print());
    }

    @After
    public void tearDown() {
        server.stop();
    }

}
