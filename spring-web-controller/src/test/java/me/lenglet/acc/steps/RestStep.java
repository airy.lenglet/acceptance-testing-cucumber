package me.lenglet.acc.steps;

import io.cucumber.java.en.Then;
import me.lenglet.acc.CucumberSpringApplicationRunner;

import static org.junit.Assert.assertEquals;

public class RestStep {

    private final CucumberSpringApplicationRunner server = new CucumberSpringApplicationRunner();

    @Then("^the client receives status code of (\\d+)$")
    public void the_client_receives_status_code_of(int statusCode) throws Throwable {
        assertEquals(statusCode, server.response().getStatusCode());
    }
}
