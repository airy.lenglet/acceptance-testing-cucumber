package me.lenglet.acc.steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import me.lenglet.acc.CucumberSpringApplicationRunner;

import static io.restassured.module.jsv.JsonSchemaValidator.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class IsAliveStep {

    private CucumberSpringApplicationRunner server;

    @Before
    public void setUp() {
        server = new CucumberSpringApplicationRunner();
        server.start();
    }

    @When("^the client calls /isAlive")
    public void the_client_issues_GET_version() throws Throwable {
        server.get("/api/isAlive");
    }



    @And("^the client receives true$")
    public void the_client_receives_server_version_body() throws Throwable {
        matchesJsonSchema(this.getClass().getResource("/json-schema/isAliveResponse.json"))
                .matches(server.response().body().print());
        assertTrue(server.response().body().jsonPath().getBoolean("isAlive"));
    }

    @After
    public void tearDown() {
        server.stop();
    }

}
