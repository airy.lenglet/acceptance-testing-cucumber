package me.lenglet.acc;

import io.restassured.response.Response;

public enum TestContext {

    CONTEXT;

    private final ThreadLocal<Response> lastResponse = new ThreadLocal<>();

    public Response response() {
        return lastResponse.get();
    }

    public void setResponse(Response response) {
        lastResponse.set(response);
    }
}
