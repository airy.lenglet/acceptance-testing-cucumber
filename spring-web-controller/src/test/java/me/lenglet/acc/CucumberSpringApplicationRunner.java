package me.lenglet.acc;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import me.lenglet.Main;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.util.SocketUtils;

public class CucumberSpringApplicationRunner {

    private static final int SUCCESS_EXIT_CODE = 0;

    private int socketPort;
    private ConfigurableApplicationContext applicationContext;

    public void start() {
        try {
            this.socketPort = SocketUtils.findAvailableTcpPort();
            this.applicationContext = SpringApplication.run(Main.class, args());
        } catch (Exception e) {
            throw new RuntimeException("Unable to start server", e);
        }
    }

    private String[] args() {
        return new String[]{
                "--server.port=" + this.socketPort,
                "--spring.datasource.url=" + dbUrl(),
                "--spring.datasource.driverClassName=org.h2.Driver",
                "--spring.datasource.username=sa",
                "--sspring.jpa.hibernate.ddl-auto=none"
        };
    }

    public void get(String path) {
        TestContext.CONTEXT.setResponse(RestAssured.get(baseUrl() + path));
    }

    public void stop() {
        try {
            SpringApplication.exit(applicationContext, () -> SUCCESS_EXIT_CODE);
        } catch (Exception e) {
            throw new RuntimeException("Unable toi stop server", e);
        }
    }

    public Response response() {
        return TestContext.CONTEXT.response();
    }

    private String dbUrl() {
        return "jdbc:h2:mem:test;INIT=RUNSCRIPT FROM '" + path("/init.sql") + "'";
    }

    private String baseUrl() {
        return "http://localhost:" + socketPort;
    }

    private String path(String resource) {
        return this.getClass().getResource(resource).getPath();
    }
}
