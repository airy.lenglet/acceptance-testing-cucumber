package me.lenglet.acc;

import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(CucumberReportRunner.class)
@CucumberOptions(features = {"src/test/resources/cucumber"}, plugin = {"pretty", "json:target/cucumber-report.json"})
public class CucumberAcceptanceTests {
}
